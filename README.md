
# Proj0-Hello
-------------

Basic Initial BitBucket/Python Project to test, clone, fork and commit git changes. 
Contains a basic python script to print out a string contained in (credentials.ini)another file

## Self
------------
Author - Isaac Perks

Original Git: "UOCIS322" proj0-hello

Contact - iperks@uoregon.edu
